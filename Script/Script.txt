game.StarterGui:SetCore("SendNotification", {
   Title = "Loading Version 0.2";
   Text = "Please Wait...";
   Duration = 3;
})

game.Players.LocalPlayer.PlayerValues.Inventory.BearSkins:ClearAllChildren()
game.Players.LocalPlayer.PlayerValues.Inventory.SocialGears:ClearAllChildren()
game.Players.LocalPlayer.PlayerValues.Inventory.KillEffects:ClearAllChildren()
local plr = game.Players.LocalPlayer
local Datas = game.ReplicatedStorage.GameContent
local PlayerData = plr.PlayerValues.Inventory

for i, ServerGears in pairs(Datas.SocialGears:GetChildren()) do
	if ServerGears.ClassName == "Folder" then
	local Sk = Instance.new("StringValue")
	Sk.Name = ServerGears.Name
	Sk.Value = ""
	Sk.Parent = PlayerData.SocialGears
	end
end
for i, ServerEffects in pairs(Datas.KillEffects:GetChildren()) do
	if ServerEffects.ClassName == "Folder" then
	local Sk = Instance.new("StringValue")
	Sk.Name = ServerEffects.Name
	Sk.Value = ""
	Sk.Parent = PlayerData.KillEffects
	end
end
for i, ServerSkins in pairs(Datas.BearSkins:GetChildren()) do
	if ServerSkins.ClassName == "Folder" then
	local Sk = Instance.new("StringValue")
	Sk.Name = ServerSkins.Name
	Sk.Value = ""
	Sk.Parent = PlayerData.BearSkins
	end
end

game.StarterGui:SetCore("SendNotification", {
	Title = "Sucess";
	Text = "All Skins, Gears and Kill Effects Unlocked. Script by ModMaker Spiki";
	Duration = 10;
})
game.StarterGui:SetCore("SendNotification", {
	Title = "WARNING";
	Text = "You can't equip unlocked assets! Its just Visual in your inventory!";
	Button1 = "OK";
	Duration = 99999;
})
--https://pastebin.com/raw/kticXpVh
