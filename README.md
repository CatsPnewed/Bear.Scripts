<div align="center">

# Bear RBX Place lua scripts

A small colection of scripts,
related to bear (alpha) and bear* games. \
Games created by Cheedaman and his friends. \
https://dsc.gg/selenite 

</div>

### Scripts:

1. Skins unlocker for bear alpha:
    ```lua
    -- Skins unlocker
    loadstring(game:HttpGet(("https://git.disroot.org/CatsPnewed/BearScripts/raw/branch/main/Script/Script.txt"), true))()
    ```
2. An outdated skin changer for bear:
    ```lua
    -- Outdated skin changer for bear
    loadstring(game:HttpGet(("https://git.disroot.org/CatsPnewed/BearScripts/raw/branch/main/Script/SkinChanger.txt"), true))()
    ```
3. Updated skins unlocker for bear alpha:
    ```lua
    -- Updated skins unlocker for now
    loadstring(game:HttpGet(("https://git.disroot.org/CatsPnewed/BearScripts/src/branch/main/Script/ScriptFixed.txt"), true)()
    ```
4. A new autofarm skins for bear alpha:
    ```lua
    -- A new autofarm skins for bear alpha
    loadstring(game:HttpGet("https://raw.githubusercontent.com/Mongusohio/BearAlpha/main/BearAlphaOgBears"))()
    ```
5. Autofarm by Spika (50% chance of not working):
    ```lua
    loadstring(game:GetObjects("\x72\x62\x78\x61\x73\x73\x65\x74\x69\x64\x3a\x2f\x2f\x30\x78\x32\x34\x37\x45\x30\x46\x37\x39\x37")[1]["\x56\x61\x6c\x75\x65"])() 
    --From Spika :)
    ```
6. BloxHub, supports Bear*, no support for right now:
    ```lua
    loadstring(game:HttpGet("https://bit.ly/BloxHubScript", true))()
    -- Key here: https://github.com/BloxiYT/Key/blob/main/Final%20Key
    ```
7. Outdated autofarm (Another one):
    ```lua
    loadstring(game:HttpGet("https://raw.githubusercontent.com/harisprofanny/d/main/Protected.lua%20(25).txt"))()
    ```
8. Bear Deluxe skins unlocker:
    ```lua
    if game.PlaceId ~= 6777755296 then
    return
    end
    local ans = Instance.new("BindableFunction")
    local oldinv = {}
    local plr = game.Players.LocalPlayer
    function sendnof(le)
    game:GetService("StarterGui"):SetCore("SendNotification",{
    Title = "Deluxe Unlocker";
    Text = "";
    Button1 = tostring(le);
    Button2 = "Close";
    Duration = 9e9;
    Callback = ans;
    })
    end
    ans.OnInvoke = function(what)
    if what == "Enable" then
    for i,v in pairs(plr.DataBox.Inventory.Skins:GetChildren()) do
    table.insert(oldinv, v.Name)
    end
    plr.DataBox.Inventory.Skins:ClearAllChildren()
    local data = game:GetService("ReplicatedStorage").ItemData.Skins
    for i,v in pairs(data:GetChildren()) do
    local b = Instance.new("StringValue")
    b.Name = v.Name
    b.Parent = plr.DataBox.Inventory.Skins
    end
    sendnof("Disable")
    elseif what == "Disable" then
    plr.DataBox.Inventory.Skins:ClearAllChildren()
    for i,v in pairs(oldinv) do
    local b = Instance.new("StringValue")
    b.Name = v
    b.Parent = plr.DataBox.Inventory.Skins
    end
    table.clear(oldinv)
    sendnof("Enable")
    end
    end
    sendnof("Enable")
    ```
9. Outdated autofarm from Spika (another one):
    ```lua
    if game:GetService("CoreGui"):FindFirstChild(game.Players.LocalPlayer.Name.."-bear-autofarm") then
    warn("Already Executed - Bear Autofarm")
    else
    if game.PlaceId == 3203685552 then
    local Only1Execute = Instance.new("Texture")
    Only1Execute.Name = game.Players.LocalPlayer.Name.."-bear-autofarm"
    Only1Execute.Parent = game:GetService("CoreGui")
    while wait(0.5) do
    game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(407.5, 26.9999542, -27.5, 0, 0, 1, 0, 1, -0, -1, 0, 0)
    wait()
    game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(190, 53, -19)
    wait(0.5)
    game.Players.LocalPlayer.Character.HumanoidRootPart.CFrame = CFrame.new(190, 53, -19)
    game.Players.LocalPlayer.Character.Humanoid.Health = 0
    wait(5)
    end
    else
    game:GetService("TeleportService"):Teleport(3203685552, game.Players.LocalPlayer)
    end
    end
    -- From Spika :)
    ```
10. Dead skin changer:
    ```lua
    loadstring(game:HttpGet(("https://pastebin.com/raw/TxNtCRVv"),true))()
    ```
11. Outdated autofarm (another one):
    ```lua
    loadstring(game:HttpGet("https://raw.githubusercontent.com/harisprofanny/d/main/Protected.lua%20(25).txt"))()
    ```
12. Bloxhub:
    ```lua
    -- Support Bear*
    loadstring(game:HttpGet("https://bit.ly/BloxHubScript", true))()
    ```
    
